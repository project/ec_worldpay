Payment Module  : ec_worldpay
Original Author : Dublin Drupaller
Settings        : administer > eCommerce configuration > receipt types > Worldpay

********************************************************************
DESCRIPTION:

This module allows you to accept payments on your Drupal eCommerce shop
using the Worldpay payment processing server (Select Junior)

Contact Dublin Drupaller via http://www.DublinDrupaller.com for assistance
or for suggestions, ideas or improvements.
********************************************************************



INSTALLATION
------------------
This version of the ec_worldpay.module requires version 6.x-4.x of the Drupal eCommerce API

Pre-installation note: After downloading the module from Drupal.org, it is recommended you
upload the ec_worldpay module files to /sites/all/modules/ecommerce/ec_worldpay

(a) Enable the module as you would any other Drupal module under ADMINISTER -> SITE BUILDING -> MODULES

(b) Once enabled go to the ec_worldpay settings page which can be found by following these links

ADMINISTER > E-COMMERCE CONFIGURATION -> RECEIPT TYPES ->

(c) Click on the Worldpay link to display the OPTIONS page and then click on the SETTINGS tab

(d) Enter in your details as provided by Worldpay when you setup your account.

(e) Once you have saved your Worldpay settings, click on the OPTIONS link to display the OPTIONS page and ensure the ALLOW PAYMENTS
option is enabled.

(f) In your Worldpay control panel you need to set the following as your Worldpay Payment Response URL (or Callback URL): http://<wpdisplay item="MC_callback">  The worldpay.module submits a dynamic worldpay_callback page along
with the order which allows you to run multiple shops using the same Worldpay account. Alternatively you can set it to be http://www.example.com/worldpay_callback if you are just
plan to use your Worldpay account for one shop.

Login to the WorldPay Merchant Administration Interface > System Settings > Integration Setup : PRODUCTION and set the Payment Response URL to:

<wpdisplay item="MC_callback">

You should also tick the "Payment Response enabled?" option.


OPTIONAL SECURITY MD5 HASH KEY
----------------------------
This is an optional security measure that passes a special hash key to the Worldpay server, generated dynamically for each order using a special
your own unique Worldpay MD5 signature that is provided to you by the Worldpay support team, to ensure the data hasn't been tampered with in between your server and the Worldpay server.

FUTURE PAY
-------------
A FuturePay option, for recurring or repeat payments, such as memberships, is coming in the next version.
If you have a project that requires the Future Pay option, please contact dub(at)dublindrupaller.com.


UNINSTALL
-------------

An uninstall routine is included with the ClickandBuy module.
(a) Go to ADMINISTER -> SITE BUILDING -> MODULES, uncheck the Worldpay module checkbox and save configuration.
(b) Click on the UNINSTALL link at the top of the MODULES page and follow the instructions to remove all references to Worldpay
in the database. (Note: This does not remove Worldpay transactions from the eCommerce database tables)
(c) Remove the ec_worldpay files and folder from your server.


TO DO
------
(a) Upgrade the module to allow for FuturePay payments with Worldpay.com.
(b) Upgrade the module to allow for the new Direct XML payment integration with Worldpay.com.


NOTES
---------
For support/assistance, or if you have any ideas for improvements, please
contact Dublin Drupaller: dub@dublindrupaller.com

